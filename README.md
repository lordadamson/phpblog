# phpblog
Creating a php MVC framework inspired by Laravel.

<img src="Screenshot_2024-06-22_at_9.37.04_PM.png ">

## Technologies used:
* Klein router
* twig template engine

## Installation and Usage
* You'll need composer to install dependencies: https://getcomposer.org/
* Clone or download this repo: `git clone https://github.com/lordadamson/phpblog.git`
* `cd phpblog`
* `composer install`
* open dbconfig with your favorite text editor and provide the needed database information.
* `php initialize_database.php`
* `./runserver.sh`
* on your browser go to `localhost:8000/posts`
