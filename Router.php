<?php

require_once 'vendor/autoload.php';

include 'Controllers/PostsController.php';

class Router
{
    public static function get($route, $controllerfunction)
    {
        $klein = new \Klein\Klein();

        $controllerfunction = explode('@', $controllerfunction);
        $controller = $controllerfunction[0];
        $function = $controllerfunction[1];

        $controller = new $controller();

        $klein->respond('GET', $route, [$controller, $function]);

        $klein->dispatch();
    }

    public static function post($route, $controllerfunction)
    {
        $klein = new \Klein\Klein();

        $controllerfunction = explode('@', $controllerfunction);
        $controller = $controllerfunction[0];
        $function = $controllerfunction[1];

        $controller = new $controller();
        
        $klein->respond('POST', $route, [$controller, $function]);

        $klein->dispatch();
    }

    public function put() {}
    public function delete() {}
    public function resource() {}
}