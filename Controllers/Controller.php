<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

Class Controller
{
	private $views = 'Views';
	private $cache = 'Cache';
	private $twig;

	public function __construct()
	{
		$loader = new Twig_Loader_Filesystem($this->views);
		$this->twig = new Twig_Environment($loader, array(
			'cache' => $this->cache,
		));
	}

	function view($view_name, $arguments)
	{
		$template = $this->twig->load($view_name . ".html");
		if (isset($arguments))
			echo $template->render($arguments);
		else
			echo $template->render(array());
	}
}