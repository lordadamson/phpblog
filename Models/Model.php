<?php

include 'dbconfig.php';

Class Model
{
    protected $table_name;

    private $servername;
    private $username;
    private $password;
    private $database_name;

    public function __construct()
    {
        $dbconfig = new DBConfig();
        $this->servername = $dbconfig->servername;
        $this->username = $dbconfig->username;
        $this->password = $dbconfig->password;
        $this->database_name = $dbconfig->database_name;
    }

    public function create_table()
    {
        try {
                $conn = new PDO("mysql:host=$this->servername;dbname=$this->database_name", $this->username, $this->password);
                // set the PDO error mode to exception
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // sql to create table
                $sql = "CREATE TABLE " . $this->table_name . " (
                id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
                content TEXT NOT NULL
                )";

                $conn->exec($sql);

                echo "Connected successfully"; 
            }
        catch(PDOException $e)
            {
                echo "Connection failed: " . $e->getMessage();
            }
    }

    public function insert()
    {

    }
}